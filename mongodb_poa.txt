https://zoom.us/meeting/register/tJMkcOqvqT0pG9JlVS5gNVrxC-rHvE0o4XIa 

https://codeinsightacademy.com/blog/database/mongodb-cheat-sheet/

https://codeinsightacademy.com/blog/javascript/dma-project-cheat-sheet/


mongosh "mongodb+srv://cluster0.dslyw.mongodb.net/myFirstDatabase" --apiVersion 1 --username root -p root

============================

Software Installation

XAMPP
https://www.apachefriends.org/download.html

VS Code
https://code.visualstudio.com/download

MongoDB Server
https://www.mongodb.com/try/download/community?tck=docs_server

MongoDB Compass
https://www.mongodb.com/try/download/compass

MongoDB Tools
https://www.mongodb.com/try/download/database-tools?tck=docs_databasetools

Git for Windows
https://gitforwindows.org/

Node.js
https://nodejs.org/en/download/

PLEASE INSTALL POSTMAN
Postman (for REST Api call)
https://www.postman.com/downloads/

Chrome Extension
https://reqbin.com/

Nodepad++
https://notepad-plus-plus.org/downloads/

Heroku cli
https://devcenter.heroku.com/articles/heroku-cli#download-and-install

MongoShell
https://www.mongodb.com/try/download/shell



====================================================
				JavaScript and JSON
====================================================
Introduction – Difference between java and javascript
Hello World in Chrome Console
console.log() function
alert() function
Variables and datatypes
Concatenation and Template Literals
Difference between x, var x and let x
document.getElementById
value vs innerHTML
functions
events: onclick onkeypress onkeydown onkeyup onmouseover
Array – One dimensional and Multi dimensional
Array Functions: forEach, map, reduce, filter, includes
JSON object
Array of JSON
lambda expression or arrow ()=>{} function in javascript


====================================================
				MongoDB
====================================================

MySQL CRUD Operations using phpmyadmin
SQL vs NoSQL
CRUD Operations in mongo shell
MongoDB Atlas
Connect using mongo shell to mongodb atlas and perform CRUD Operations


show dbs;
db;
show collections;
db.createCollection("users");
var doc = {
	"name": "Ajit",
	"age": 20,
	"city": "Mumbai"
}
db.users.insertOne(doc);

var docs = [
{"name":"nehav","contact_number":"9833910534","address":"mumbai","salary":30000,"employee_id":98821,"role":"manager"},
{"name":"mina","contact_number":"9833910535","address":"thane","salary":32000,"employee_id":98823,"role":"sales"},
{"name":"pankaj","contact_number":"9833910536","address":"bhopal","salary":40000,"employee_id":98824,"role":"hr"},
{"name":"mareena","contact_number":"9833910537","address":"meerut","salary":45000,"employee_id":98825,"role":"support"},
{"name":"pooja","contact_number":"9833910538","address":"delhi","salary":50000,"employee_id":98826,"role":"developer"},
{"name":"namita","contact_number":"9833910539","address":"surat","salary":52000,"employee_id":98820,"role":"sales"},
{"name":"sneha","contact_number":"9833910510","address":"baroda","salary":55000,"employee_id":98827,"role":"support"},
{"name":"anjali","contact_number":"9833910511","address":"ahmedabad","salary":60000,"employee_id":98828,"role":"tester"},
{"name":"harsha","contact_number":"9833910512","address":"mumbai","salary":20000,"employee_id":98829,"role":"operations"},
{"name":"varun","contact_number":"9833910512","address":"mehsana","salary":56000,"employee_id":98831,"role":"tester"},
{"name":"preeti","contact_number":"9833910513","address":"noida","salary":87000,"employee_id":98832,"role":"developer"},
{"name":"madhu","contact_number":"9833910525","address":"bangalore","salary":22000,"employee_id":98833,"role":"sales"}
];

db.users.insertMany(docs);

db.users.find();
db.users.find({query});
atad
db.users.find().limit(3);
db.users.find().skip(3);
db.users.find().skip(3).limit(5);
db.users.count();
db.users.find().order({"salary": 1});
db.users.find().order({"salary": -1});
db.users.find({"name" : /a/});
db.users.find({"name" : /^a/});
db.users.find({"name" : /a$/});
db.users.find({"name" : /[aeiou]$/});
db.users.find({"name" : /[^aeiou]$/});

------------------
SHORT BREAK 5 MINS
------------------
===================
	Operators
===================
eq
db.users.find({"address" : { "$eq" : "mumbai" }});

neq
db.users.find({"address" : { "$ne" : "mumbai" }});

greater than
db.users.find({"salary" : { "$gt" : 50000 }});

less than
db.users.find({"salary" : { "$lt" : 50000 }});

in
db.users.find({"address" : { "$in" : ["delhi", "mumbai", "pune"]}});

and
db.users.find({"$and" : [{"salary" : { "$gt" : 50000 }}, {"salary" : { "$lt" : 70000 }}]});

or
db.users.find({"$or" : [{"address" : "delhi"}, {"address" : "mumbai"}]});

===================
	Aggregation
===================

db.users.aggregate([{
	$group : {
		"_id" : "kuchbhi",
		"sum_salary" : { "$sum" : "$salary" }
	}
}]);

db.users.aggregate([{
	$group : {
		"_id" : "kuchbhi",
		"avg_salary" : { "$avg" : "$salary" }
	}
}]);

db.users.aggregate([{
	$group : {
		"_id" : "kuchbhi",
		"max_salary" : { "$max" : "$salary" }
	}
}]);

db.users.aggregate([{
	$group : {
		"_id" : "kuchbhi",
		"min_salary" : { "$min" : "$salary" }
	}
}]);

All together

db.users.aggregate([{
	$group : {
		"_id" : "kuchbhi",
		"sum_salary" : { "$sum" : "$salary" },
		"avg_salary" : { "$avg" : "$salary" },
		"max_salary" : { "$max" : "$salary" },
		"min_salary" : { "$min" : "$salary" }
	}
}]);

-------------------
SHORT BREAK 10 MINS
-------------------
===================
UPDATE AND DELETE
===================
updateOne
db.users.updateOne({"name" : "nehav"}, { $set : {"name" : "neha"} });

updateMany
db.users.updateMany({"address" : "mumbai"}, { $set : {"salary" : 80000} });

deleteOne
db.users.deleteOne({"_id" : "123456789"});

deleteMany
db.users.deleteMany({"address" : "pune"});

show collections;
db.users.drop();



====================================================
				Node.js
====================================================
https://codeinsightacademy.com/blog/javascript/nodejs-cheat-sheet/
====================================================

console.log("helloworld") in app.js file
herdcoded square of number
dynamic read number using CLA (command line argument) process.argv[3]
create function in app.js file
Modules - create module calculate_functions.js having functions add, sub, mul, div - import the same in app.js
Express Framework - CRUD Operations - (add, sub, mul, div)

==========================
app.js
==========================
const express = require("express");
const port = 8081;
const app = express();
app.use(express.json());


app.get("/", (req, res) => {
	console.log(req.query);
	console.log(req.query.name);
	nm = req.query.name;

	res.send(`Hello ${nm}..`);
});


app.listen(process.env.PORT || port, () => {
	console.log(`Listening on port ${port}`);
});

===========TO RUN APP==============
node app.js

npm i nodemon
OR
npm i -g nodemon

./node_modules/nodemon/bin/nodemon.js app.js
===================================


=====GREATEST AMONG 2 NUMBER======
app.get("/greatest", (req, res) => {

	x = req.query.num1;
	y = req.query.num2;

	output = "";

	if(x > y)
		output = `${x} is greatest`
	else
		output = `${y} is greatest`

	res.send(output);

});
====================================


==========TABLE OF NUMBER ==========

app.get("/table", (req, res) => {
	num = req.query.num 

	output = "";

	for(i = 1; i <= 10; i++) {
		output = output + `${num} * ${i} = ${num * i}`;
		output = output + `&nbsp; &nbsp; &nbsp &copy;`;
		output = output + `${num} * ${11 - i} = ${num * (11 -i)}`;
		output = output + `<br>`;
	}

	res.send(output);

});

====================================


======READ AND WRITE JSON ===========
app.post("/calc", (req, res) => {
	console.log(req.body);

	x = req.body.num1;
	y = req.body.num2;

	sum = x + y
	sub = x - y
	mul = x * y
	div = x / y
	mod = x %y

	data = [
		{"sum" : sum},
		{"sub" : sub},
		{"mul" : mul},
		{"div" : div},
		{"mod" : mod}
	]

	res.send(data);

});

======================================


====================================================
		DAY 4	Mongoose MongoDB Connection
====================================================
Mongoose Module - Database connection and CRUD operations - Use Postman






HTML - Table, Forms - Perform CRUD Operations using Javascript fetch function



====================================================
			Development and Deployment
====================================================
Create and Deployment Mongo Express VanillaJS Node.js Stack
1. github account (gitlab account)
2. heroku
3. MongoDB Atlas Cloud
4. Deployment
5. Testing


====================================================
			Git Commands
====================================================
git clone url
cd folder
git add .
git commit -m "Initital Commit"
git push origin main 
OR 
git push origin master

Referesh the browser and check files

====================================================
			MAP REDUCE
====================================================

array map function in javascript
var nums = [1, 2, 3, 4, 5];
var sqr_of_nums = nums.map(function(x) { return x * x});
]

array reduce function in javascript
var nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
var initial_value = 0;
var sum_of_nums = nums.reduce(function(total, x) { return total + x}, initial_value);
var sum_of_nums = nums.reduce(function(total, x) { return total + x}, 100);

====================

mapReduce in MongoDB
mapReduce work together in MongoDB
emitted fields from mapFunction will assign to key, value of reduce function


var mapFun = function() { emit(this.address, 1) };
var redFun = function(key, val) { return Array.sum(val) };
db.users.mapReduce(mapFun, redFun, { "out" : "output" });
db.output.find();


emitted fields
~~~~~~~~~~~~~
address | count	|
-----------------
mumbai	| 	1	|
mumbai	| 	1	|
nagpur	| 	1	|
pune	| 	1	|
-----------------

final output:-
~~~~~~~~~~~~~
{
	"mumbai" : 2,
	"nagpur" : 1,
	"pune"	 : 1
}

var mapFun = function() {emit(this.address, this.salary)};
var redFun = function(key, val) { return Array.sum(val) };
db.users.mapReduce(mapFun, redFun, { "out" : "output" });
db.output.find();


========================================
							THANK YOU
========================================